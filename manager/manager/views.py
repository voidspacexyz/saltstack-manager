from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, login, logout
from api import pki, minion_status
import salt.client
import salt.wheel
from rest_framework import viewsets
from manager.serializers import UserSerializer, GroupSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

def index(request):
    if request.user.is_authenticated():
        return redirect("/dashboard/")
    else :
        return render(request, "manager/login.html")

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect("/dashboard/")
        else:
            print("The password is valid, but the account has been disabled!")
    else:
        print("The username and password were incorrect.")
        return redirect("/")

def logout_view(request):
	logout(request)
	return redirect("/")

@login_required(login_url="/")
def dashboard(request):
    pki.get_salt_master_config()
    cfg, err = pki.get_salt_master_config()
    if err:
        raise Exception(err)
    opts = salt.config.master_config(cfg)
    wheel = salt.wheel.Wheel(opts)
    request.minion_status, err = minion_status.get_minions_status()
    request.accepted_minions, err = pki.get_accepted_minions()
    request.pending_minions, err = pki.get_pending_minions()
    if err:
        raise Exception(err)
    print request.minion_status
    return render(request, "manager/dashboard.html")


@login_required(login_url="/")
def accept_minion(request):
    pki.get_salt_master_config()
    cfg, err = pki.get_salt_master_config()
    if err:
        raise Exception(err)
    opts = salt.config.master_config(cfg)
    wheel = salt.wheel.Wheel(opts)
    m = request.GET.get("m", None)
    status, err = pki.accept_minion(wheel, m)
    if err:
        raise Exception(err)
    return redirect("/")


@login_required(login_url="/")
def delete_minion(request):
    m = request.GET.get("m", None)
    if m:
        status, err = pki.delete_minion(m)
        if err:
            raise Exception(err)
        return redirect("/")
    else:
        raise Exception("Minion id not received")


@login_required(login_url="/")
def get_minion_details(request):
    m = request.GET.get("m", None)
    if m:
        request.minion_grains_info, err = minion_status.get_grains_info(m)
        if err:
            raise Exception(err)
        data, err = minion_status.get_cpu_info(m)
        if err:
            raise Exception(err)
        request.minion_cpu_info = data
        request.raminfo = {
            "total": int(data["memtotal"]["value"])/1048576,
            "available": int(data["memfree"]["value"])/1048576
        }
        request.diskinfo = {
            "root":{
                "available" : data["diskusage"]["/"]["available"]/1084227584,
                "total": data["diskusage"]["/"]["total"]/1084227584
            },
            "home":{
                "available" : data["diskusage"]["/home"]["available"]/1084227584,
                "total": data["diskusage"]["/home"]["total"]/1084227584
            },
            "temp":{
                "available" : data["diskusage"]["/tmp"]["available"]/1084227584,
                "total": data["diskusage"]["/tmp"]["total"]/1084227584
            }
        }
        return render(request, "manager/miniondetails.html")
    else:
        raise Exception("Minion id not received")
