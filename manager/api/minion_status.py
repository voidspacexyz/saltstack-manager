import salt.client
import salt.runner


opts = salt.config.master_config('/etc/salt/master')
runner = salt.runner.RunnerClient(opts)


"""
This function will remove the up/down status of all the minions for a master.
"""


def get_minions_status():
    try:
        minions = runner.cmd('manage.status')
        return minions, None
    except Exception as e:
        print "Exception occured: " + str(e)
        return None, str(e)


"""
Get any rquired information about cpu of a specific minion
"""


def get_cpu_info(minion):
    try:
        result = {}
        local = salt.client.LocalClient()
        load = local.cmd(minion, 'status.loadavg')
        disk = local.cmd(minion, 'status.diskusage')
        mem_info = local.cmd(minion, 'status.meminfo')
        result['loadavg'] = load[minion]
        result['diskusage'] = disk[minion]
        result['memfree'] = mem_info[minion]['MemFree']
        result['memtotal'] = mem_info[minion]['MemTotal']
        return result, None
    except Exception as e:
        print "Exception occured: " + str(e)
        return None, str(e)


def get_grains_info(minion):
    try:
        local = salt.client.LocalClient()
        result = local.cmd(minion,
                           'grains.item',
                           ['kernelrelease',
                            'kernel',
                            'fqdn',
                            'ip4_interfaces',
                            'cpumodel',
                            'cpuarch',
                            'lsb_distrib_description',
                            'saltversion',
                            'productname'])
        return result[minion], None
    except Exception as e:
        print "Exception occured: " + str(e)
        return None, str(e)


minion = 'voidspace.xyz'
print get_minions_status()
print get_cpu_info(minion)
print get_grains_info(minion)
