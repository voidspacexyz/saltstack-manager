import salt.client
import salt.wheel

import socket
import time

"""
The salt master file
"""


def get_salt_master_config():
    return '/etc/salt/master', None
"""
The salt minion file
"""


def get_salt_minion_config():
    return '/etc/salt/minion', None


cfg, err = get_salt_master_config()
if err:
    raise Exception(err)
opts = salt.config.master_config(cfg)
wheel = salt.wheel.Wheel(opts)


def get_accepted_minions():
    minion_list = []
    try:
        keys = wheel.call_func('key.list_all')
        if keys:
            minion_list = keys['minions']
    except Exception as e:
        return None, "Error retrieving accepted minions : %s" % str(e)
    else:
        return minion_list, None


def get_pending_minions():
    pending_minions = None
    try:
        keys = wheel.call_func('key.list_all')
        if keys:
            pending_minions = keys['minions_pre']
    except Exception as e:
        return None, 'Error retrieving pending minions : %s' % str(e)
    else:
        return pending_minions, None


def accept_minion(wheel, m):
    ret = -1
    try:
        if not wheel or not m:
            raise Exception('Required parameters not passed')
        if not wheel.call_func('key.accept', match=('%s' % m)):
            raise Exception('Minion %s' % m)
        else:
            ret = 0
    except Exception as e:
        return False, 'Error accepting Minion with key : %s' % str(e)
    else:
        return True, None


def delete_minion(hostname):
    try:
        keys = wheel.call_func('key.list_all')
        if (not keys) or ('minions' not in keys) or (
                hostname not in keys['minions']):
            raise Exception("Specified GRIDCell is not part of the grid")
        wheel.call_func('key.delete', match=(hostname))
    except Exception as e:
        errors = "Error removing GRIDCell key from the grid : %s" % str(e)
        return False, errors
    else:
        return True, None


def main():
    print get_pending_minions()
    print accept_minion(wheel, "ms-1")
    print get_accepted_minions()
    print delete_minion("ms-1")
    print get_accepted_minions()

if __name__ == "__main__":
    main()
