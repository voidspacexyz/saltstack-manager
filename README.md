## SaltStack Manager

SaltStack is a configaration management and infrastructure management framework, which is also entirely written in Python.SaltStack is mostly managed from the commandline or people write their own webapp to manage the configurations.

SaltStack Manager is a webapp that lets us manage SaltStack.
Core Functionalities include

-  Accept, Reject and manange minions
-  Get the status of each of the minions.
-  Execute SaltStack Modules on all or selective minions
-  Maintiain minions in a salt state
-  Multi-Master configuration
-  SaltStack syndic 
and more.


RoadMap :
v1.0
 - Ability to add,reject and manage minions
 - Get information of each minion, including system graphs.

v2.0
 - Ablity to execute saltstack modules
 - Ability to to saltstack state


